package org.kllbff.expelledstudents;

import android.graphics.Rect;
import android.text.TextPaint;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class Utils {
    public static void changeSize(View view, int size) {
        ViewGroup.LayoutParams params = view.getLayoutParams();
        params.width = params.height = size;
        view.setLayoutParams(params);
    }

    public static void addSize(View view, int size) {
        ViewGroup.LayoutParams params = view.getLayoutParams();
        params.width = (params.height += size);
        view.setLayoutParams(params);
    }

    public static int zeroSymbolWidth(TextView textView) {
        TextPaint paint = textView.getPaint();
        Rect bounds = new Rect();
        paint.getTextBounds("0", 0, 1, bounds);
        return bounds.width();
    }
}
