package org.kllbff.expelledstudents;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.snackbar.Snackbar;

public class MainActivity extends AppCompatActivity {
    private static final String COUNTER_KEY = "studentsCounter";
    private static final String LAST_SIZE_KEY = "lastCounterSize";
    private Integer counter;
    private Integer coefficient;
    private TextView counterView;
    private EditText perClickCountView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        counterView = findViewById(R.id.counter);
        perClickCountView = findViewById(R.id.students_per_click_count);

        coefficient = 10;
        if(savedInstanceState != null) {
            counter = savedInstanceState.getInt(COUNTER_KEY, 0);

            int size = savedInstanceState.getInt(LAST_SIZE_KEY, -1);
            if(size > 0) {
                Utils.changeSize(counterView, size);
            }

            while(coefficient < counter) {
                coefficient *= 10;
            }
        } else {
            counter = 0;
        }
        updateCounterView();
    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putInt(COUNTER_KEY, counter);
        bundle.putInt(LAST_SIZE_KEY, counterView.getWidth());
    }

    private void updateCounterView() {
        if(counter >= coefficient) {
            ExpandAnimation anim = new ExpandAnimation(this, counterView);
            counterView.startAnimation(anim.getCollapse());
            coefficient *= 10;
        }

        counterView.setText(counter.toString());
    }

    public void expelStudent(View view) {
        String value = perClickCountView.getText().toString();
        Integer expelCount;

        if(value.isEmpty()) {
            expelCount = 1;
        } else {
            expelCount = Integer.valueOf(value);
        }

        counter += expelCount;

        updateCounterView();
        Snackbar.make(findViewById(R.id.root), getString(R.string.expelled_n_students, expelCount),
                Snackbar.LENGTH_SHORT).show();
    }
}
