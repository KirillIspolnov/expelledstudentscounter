package org.kllbff.expelledstudents;

import android.content.Context;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

public class ExpandAnimation implements Animation.AnimationListener {
    private TextView view;
    private Animation collapse, expand;

    public ExpandAnimation(Context ctx, TextView view) {
        collapse = AnimationUtils.loadAnimation(ctx, R.anim.collapse);
        collapse.setAnimationListener(this);

        expand = AnimationUtils.loadAnimation(ctx, R.anim.expand);
        expand.setAnimationListener(this);

        this.view = view;
    }

    public Animation getCollapse() {
        return collapse;
    }

    public Animation getExpand() {
        return expand;
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {
        if(animation.equals(collapse)) {
            view.startAnimation(expand);
        }
        if(animation.equals(expand)) {
            Utils.addSize(view, Utils.zeroSymbolWidth(view));
        }
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}
